const mongoose = require('mongoose');

const assmtsSchema = new mongoose.Schema({
  name: String,
  questions: Array
});

const assmts = mongoose.model('assessments', assmtsSchema);

exports = module.exports = assmts;
