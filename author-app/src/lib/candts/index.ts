
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms'

import { CandtDetailComponent } from './candt-detail';
import {CandtListComponent, CandtQuestListComponent} from './candt-list'
import {CandtFormComponent} from './candt-form'

@NgModule({
  imports: [BrowserModule, ReactiveFormsModule],
  exports: [CandtListComponent, CandtDetailComponent, CandtQuestListComponent, CandtFormComponent],
  declarations: [CandtDetailComponent, CandtListComponent, CandtQuestListComponent, CandtFormComponent],
  providers: [],
})
export class CandtModule { }
