
import { Component, OnInit } from '@angular/core';

import {DetailComponent} from '../base/comps/detail'

@Component({
  selector: 'quest-detail',
  templateUrl: '../base/comps/detail/detail.html'
})

export class QuestDetailComponent extends DetailComponent {
  
  fieldsets  = [
    [{label: 'Question', name: 'question'}],
    [{label: 'Category', name: 'category'}]
  ]

}