const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
  name: String,
  username: String,
  password: String
});

const users = mongoose.model('users', usersSchema);

exports = module.exports = users;
