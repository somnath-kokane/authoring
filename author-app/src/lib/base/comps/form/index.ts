import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'qa-form',
  templateUrl: 'form.html'
})

export class FormComponent {
  
  form:FormGroup

  @Input() item:any
  @Output() onSubmited = new EventEmitter<any>()
  @Output() onCanceled = new EventEmitter<any>()

  fields:any

  fieldsets:any

  can = {submit: true}

  constructor(
    private fb:FormBuilder
  ) { 

    this.form = fb.group({})
  }

  ngOnInit(){
    for(let fieldName of Object.keys(this.fields)){
      const field = this.fields[fieldName]
      field.type = field.type || 'text'
      let state:any = {
        value: field.value || '',
        disabled: !!field.disabled  
      }
      let validators = []

      if(field.required){
        validators.push(Validators.required)
      }
      
      let control = new FormControl(state, validators)
      this.form.setControl(fieldName, control)
    }

    const fieldsets = []

    for(let fields of this.fieldsets){
      let _fields = []
      if(typeof fields === 'string'){
        fields = [fields]
      }
      for(let name of fields){
        if(this.fields[name]){
          let field = this.fields[name]
          _fields.push({name, ...field})
        }
      }
      if(_fields.length){
        fieldsets.push(_fields)
      }
    }

    if(fieldsets.length){
      this.fieldsets = fieldsets
    }
  }

  ngOnChanges({item}){
    if(item && item.previousValue !== item.currentValue){
      this.form.reset()
      if(item.currentValue){
        this.setValues(item.currentValue)
      }
    }
  }

  setValues(item){
    let fields = Object.keys(this.fields)
    for(let fieldName of fields){
      if(fieldName in item){
        this.form.get(fieldName).patchValue(item[fieldName])
      }
    }
  }

  onCancel(){
    this.form.reset()
    this.item = undefined
    this.onCanceled.emit()
  }

  onSubmit(){
    const item = this.form.value
    let defaultValues = {}
    for(let fieldName of Object.keys(this.fields)){
      let field = this.fields[fieldName]
      if('value' in field){
        defaultValues[fieldName] = field.value
      }
    }
    if(this.item){
      item._id = this.item._id
    }
    this.onSubmited.emit({...defaultValues, ...item})
    this.form.reset()
  }

  getClasses(count){
    let className = `col-md-${12/count}`
    return {[className]: true}
  }
}