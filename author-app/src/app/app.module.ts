import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, NgModule } from '@angular/core';
import {HttpModule} from '@angular/http'
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms'
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { TabsModule } from 'ngx-bootstrap/tabs';

import {AuthorModule} from '../lib'
import {EntryApp} from '../lib/author'

import {ALL_ROUTES} from '../lib/routes'

@NgModule({
  declarations: [EntryApp],
  imports: [
    BrowserModule,
    HttpModule,
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    TabsModule.forRoot(),
    AuthorModule,
    RouterModule.forRoot(ALL_ROUTES)
  ],
  providers: [],
  entryComponents: [EntryApp]
})
export class AppModule { 
  constructor(private _appRef: ApplicationRef) { }

  ngDoBootstrap() {
    this._appRef.bootstrap(EntryApp);
  }
}
