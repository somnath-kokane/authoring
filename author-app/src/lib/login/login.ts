import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Observable } from 'rxjs/Observable';

import {AppService} from '../services/app.service'

@Component({
  selector: 'app-login',
  templateUrl: 'login.html',
  styles: []
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup
  user:Observable<any>
  app:Observable<any>

  constructor(
    public loginModalRef:BsModalRef,
    private appService: AppService,
    private fb:FormBuilder

  ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
    this.app = this.appService.stateChanges
   }

  resetLoginForm(){
    this.loginForm.reset()
  }

  onSubmitLogin(){
    const user = this.loginForm.value
    this.loginForm.reset()
    this.loginModalRef.hide()
    this.appService.login(user)
  } 

  ngOnInit() {
  }

}
