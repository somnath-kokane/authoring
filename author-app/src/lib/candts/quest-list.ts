import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import { Observable } from 'rxjs/Observable';
import {combineLatest} from 'rxjs/observable/combineLatest'
import 'rxjs/add/observable/of'
import {CandtService} from '../services/candt.service'

@Component({
  selector: 'candt-quest-list',
  templateUrl: 'quest-list.html'
})

export class CandtQuestListComponent implements OnInit {
  subscriptions = []
  items:Observable<any>
  questions:Observable<any>
  selected:any
  form:FormGroup

  constructor(
    private service: CandtService,
    private fb:FormBuilder
  ) { 

    this.form = fb.group({
      assmtId: ['', Validators.required],
      candtId: ['', Validators.required]
    })

    this.items = this.service.stateChanges
    this.questions = combineLatest(
      this.form.valueChanges,
      this.items, 
      (value, items) => {
        let {assmtId, candtId} = value
        return (items || [])
          .filter(item => {
            if(item._id === candtId){
              for(let [key] of item.assessments){
                if(key === assmtId){
                  return true
                }
              }
            }
          })
      }
    )

    this.subscriptions.push(
      this.service.getAll().subscribe()
    )
  }

  onSelect(assmt){
    this.selected = assmt
  }

  onCancel(){
    if(this.selected){
      this.selected = null
    }
    this.form.reset()
  }

  
  onSubmit(){
    const assmt = this.form.value
    let createOrUpdate
    if(this.selected){
      createOrUpdate = this.service.update(this.selected._id, assmt)
      this.selected = null
    } else {
      createOrUpdate = this.service.create(assmt)
    }
    this.subscriptions.push(
      createOrUpdate.subscribe(res => {
        this.form.reset()
      })
    )
  }

  onEdit(item){
    this.selected = item
    let {name, username, password, isAdmin, role} = item
    this.form.setValue({name, username, password, isAdmin, role})
  }

  onDelete(item){
    this.subscriptions.push(
      this.service.remove(item._id).subscribe(res => {
        if(this.selected && this.selected._id === item._id){
          this.selected = null
        }
      })
    )
  }


  ngOnDestroy(){
    for(let subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }

  ngOnInit() { }
}