import { Component, OnInit } from '@angular/core';

import {ListComponent} from '../base/comps/list'

@Component({
  selector: 'user-list',
  templateUrl: '../base/comps/list/list.html'
})

export class UserListComponent extends ListComponent {
  displayFields = [
    {label: 'Name', name: 'name'}
  ]
}