const mongoose = require('mongoose');

const questsSchema = new mongoose.Schema({
  question: String,
  categoryId: String,
  category: String
});

const quests = mongoose.model('questions', questsSchema);

exports = module.exports = quests;
