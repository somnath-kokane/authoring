import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {RestService} from './rest.service'

@Injectable()
export class ApiService extends RestService {
  state = {loggedIn: false}
  
  setState(state){
    this.store.next({...state})
  }

  protected _afterPost(res){
    //
  }

  login(uri, user){
    return this.post(uri, user)
      .do(res => {
        let {token} = res
        localStorage.setItem('token', token)
        this.setState({loggedIn: true, token})
      })
  }

  logout(uri){
    return this.post(uri, {})
      .do(res => {
        localStorage.removeItem('token')
        this.setState({loggedIn:false})
      })
  }

}