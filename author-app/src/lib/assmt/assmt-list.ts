import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs/Observable';

import {ListComponent} from '../base/comps/list'

@Component({
  selector: 'assmt-list',
  templateUrl: '../base/comps/list/list.html'
})

export class AssmtListComponent extends ListComponent {
  displayFields = [
    {label: 'Name', name: 'name', select: true}
  ]
}