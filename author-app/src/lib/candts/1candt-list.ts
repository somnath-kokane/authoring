import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import { Observable } from 'rxjs/Observable';

import {CandtService} from '../services/candt.service'

@Component({
  selector: 'candt-list',
  templateUrl: 'candt-list.html'
})

export class CandtListComponent implements OnInit {
  subscriptions = []
  items:Observable<any>
  selected:any
  form:FormGroup

  constructor(
    private service: CandtService,
    private fb:FormBuilder
  ) { 

    this.form = fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators],
      isAdmin: [{value: false, disabled: true}, Validators.required],
      role: [{value: 'Candidate', disabled: true}, Validators.required]
    })

    this.items = this.service.stateChanges
    this.subscriptions.push(
      this.service.getAll().subscribe()
    )
  }

  onSelect(item){
    this.selected = item
  }

  onCancel(){
    if(this.selected){
      this.selected = null
    }
    this.form.reset()
  }

  
  onSubmit(){
    const user = this.form.value
    let createOrUpdate
    if(this.selected){
      createOrUpdate = this.service.update(this.selected._id, {user})
      this.selected = null
    } else {
      createOrUpdate = this.service.create({user})
    }
    this.subscriptions.push(
      createOrUpdate.subscribe(res => {
        this.form.reset()
      })
    )
  }

  onEdit(item){
    this.selected = item
    let {name, username, password, isAdmin, role} = item.user
    this.form.setValue({name, username, password, isAdmin, role})
  }

  onDelete(item){
    this.subscriptions.push(
      this.service.remove(item._id).subscribe(res => {
        if(this.selected && this.selected._id === item._id){
          this.selected = null
        }
      })
    )
  }


  ngOnDestroy(){
    for(let subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }

  ngOnInit() { }
}