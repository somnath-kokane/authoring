
import { Routes } from '@angular/router'

import { AuthorApp } from './author'
import {Home} from './pages/home'
import {Assmt} from './pages/assmt'
import {CandtPage} from './pages/candt'
import {QuestPage} from './pages/quest'

export const APP_ROUTES: Routes = [
  {
    path: '', component: AuthorApp, children: [
      { path: '', component: Home },
      { path: 'assmts', component: Assmt},
      { path: 'candts', component: CandtPage },
      { path: 'quests', component: QuestPage}
    ]
  },
]

export const ALL_ROUTES: Routes = [
  {path: '',  component: AuthorApp, children: APP_ROUTES}
];