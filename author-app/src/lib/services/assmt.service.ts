import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import {RestService} from './rest.service'
import {AppService} from './app.service'

@Injectable()
export class AssmtService extends RestService {
  constructor(
    http: Http,
    private appService: AppService
    ) {
    super(http)
  }

  getAllQuestions(item){
    return this.get(`/api/assessments/${item._id}/questions`, null, (res) => null)
  }

  setQuestions(assmtId, questions) {
    return this.put(`/api/assessments/${assmtId}/questions`, {questions}, null, (res) => {
      let assmt = (this.store.value || []).find(item => item._id === assmtId)
      if(assmt){
        assmt.questions = questions
      }
    })
  }

  getAll(){
    return this.get('/api/assessments')
  }

  create(assmt){
    return this.post('/api/assessments', assmt)
  }

  update(id, assmt){
    return this.put(`/api/assessments/${id}`, assmt)
  }

  delete(id){
    return this.remove(`/api/assessments/${id}`)
  }
  
  
}