import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import {AppService} from './services/app.service'
import {LoginComponent} from './login/login'

@Component({
  selector: 'entry-app',
  template: '<router-outlet></router-outlet>'
})

export class EntryApp implements OnInit {
  
  constructor() { }

  ngOnInit() { }
}

@Component({
  selector: 'author-app',
  templateUrl: 'author.html'
})

export class AuthorApp implements OnInit {

  app:Observable<any>
  loginModalRef:BsModalRef
  show = false
  navItems = [
    {name: 'Assessments', route: '/assmts'},
    {name: 'Questions', route: '/quests'},
    {name: 'Candidates', route: '/candts'}
  ]

  constructor(
    private appService: AppService,
    private modalService: BsModalService
  ) {
    const userString = localStorage.getItem('user')
    if(userString){
      const user = JSON.parse(userString)
      this.appService.setUser(user)
    }

    this.app = this.appService.stateChanges
   }

  ngOnInit() { }

  onLogin(ev){
    ev.preventDefault()
    this.loginModalRef = this.modalService.show(LoginComponent, {class: 'modal-sm'})
  }

  onLogout(ev){
    ev.preventDefault()
    this.appService.logout()
  }
}