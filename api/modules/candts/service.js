
const _ = require('lodash')
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Types

const Service = require('../../base/service')
const Model = require('./model')
const assmtService = require('../assmts/service')
const questService = require('../quests/service')
const userService = require('../users/service')

const service = Service(Model, {create, getAll, getAssessments, getQuestions, setAssmts, setAnswers, getQuests})

module.exports = exports = service

function _create(body, callback){
  let data = Object.assign({_id: body._id})
    Model.create(data, (err, doc) => {
      if(err){
        callback(err)
      } else {
        callback(null, Object.assign(data, body))
      }
    })
}

function create(body, callback) {
  let {username} = body
  userService.getBy({username}, (err, doc) => {
    if(err){
      callback(err)
    } else {
      console.log('user by username', doc)
      if(doc && doc._id){
        body._id = doc._id
        let userId = String(doc._id)
        userService.update(userId, body, (err, doc) => {
          if(err){
            callback(err)
          } else {
            console.log('update user', doc)
            service.get(userId, (err, doc) => {
              if(err){
                callback(err)
              } else {
                console.log('get candidate', doc)
                if(doc && doc._id){
                  service.update(doc._id, body, (err, doc) => {
                    if(err){
                      callback(err)
                    } else {
                      callback(null, Object.assign({_id: doc._id}, body))
                    }
                  })
                } else {
                  console.log('create candidate', body)
                  _create(body, callback)
                }
                
              }
            })
          }
        })
      } else {
        userService.create(body, (err, doc) => {
          body._id = doc._id
          let userId = doc._id
          service.get(userId, (err, doc) => {
            if(err){
              callback(err)
            } else {
              if(doc && doc._id){
                service.update(userId, body, (err, doc) => {
                  if(err){
                    callback(err)
                  } else {
                    callback(null, Object.assign({_id: doc._id}, body))
                  }
                })
              } else {
                _create(body, callback)
              }
            }
          })
          
        })
      }
    }
  })  
  
}

function getAll(conditions={}, callback){
  
  Model.find(conditions, (err, docs) => {
    if(err){
      callback(err)
    } else {
      //_.pick(doc, '_id', 'name', 'username')
      const values = []
      const keys = []
      const ids = docs.map(doc => {
        keys.push(String(doc._id))
        values.push(_.clone(doc))
        return ObjectId(doc._id)
      })
      userService.getAll({_id: {$in: ids}}, (err, docs) => {
        if(err){
          callback(err)
        } else {
          //  console.log('candts\n\n', docs, values)
          const candtId = []
          docs.forEach(doc => {
            let index = keys.indexOf(String(doc._id))
            
            if(index !== -1){
              const value = values[index]
              value.user = doc
              console.log('vv', value)
            }
          })
          // console.log('values\n', values)
          callback(null, values)
        }
      })

    }
  })
}

function setAnswers(candtId, assessmentId, questions, callback){
  service.get(candtId, (err, doc) => {
    if(err){
      callback(err)
    } else {
      let docQuestions = (doc.questions || []).filter(item => item.assessmentId === assessmentId)
      let ids = docQuestions.map(item => String(item._id))
      console.log('ids', ids)
      questions.forEach(item => {
        console.log('item', item)
        let idx = ids.indexOf(String(item._id))
        if(idx === -1){
          docQuestions.push(item)
        } else {
          Object.assign(docQuestions[idx], item)
        }
      })
      console.log('docQuestions', docQuestions)
      service.update(candtId, {questions: docQuestions}, callback)
      // (questions || []).forEach(item => {
      //   let idx = ids.indexOf(item._id)
      //   if(idx === -1){
      //     doc.questions.push(item)
      //   } else {
      //     doc.questions[idx].answer = item.answer
      //   }
      // })
      // service.update(candtId, {questions: doc.questions}, callback)
    }
  })
}

function setAssmts(id, assessments, callback){
  console.log('id------', id, assessments)
  service.update(id, {assessments}, callback)
}

function getAssessments(candtId, callback){
  service.get(candtId, (err, doc) => {
    
    const assmtIds = doc.assessments.map(key => ObjectId(key))
    assmtService.getAll({_id: {$in: assmtIds}}, callback)
  })
}

function getQuests(candtId, callback){
  service.get(candtId, (err, doc) => {
    if(err){
      callback(err)
    } else {
      let questions = doc.questions
      callback(null, questions)
    }
  })
}

function getQuestions(candtId, assessmentId, callback){
  assmtService.getQuestions(assessmentId, (err, docs) => {
    if(err){
      callback(err)
    } else {
      console.log('docs.........', docs)
      service.get(candtId, (err, doc) => {
        if(err){
          callbac(err)
        } else {
          console.log('doc..........', doc)
          let results = docs.map(item => {
            let {_id, question, category} = item
            return {_id, question, category, assessmentId}
          })
          let questions = (doc.questions || []).filter(quest => quest.assessmentId === assessmentId)
          let ids = (questions || []).map(item => String(item._id))
          results.forEach(item => {
            let idx = ids.indexOf(String(item._id))
            if(idx !== -1){
              item.answer = questions[idx].answer
            }
          })
          callback(null, results)
        }
      })
    }
  })
}

function _getQuestions(candtId, assmtId, callback) {
  service.get(candtId, (err, doc) => {
    if(err){
      callback(err)
    } else {
      const [key, questions=[]] = doc.assessments.find(([key]) => key === assmtId) || []
      
      const qIdxs = []
      const qIds = questions.map(([key], i) => {
        qIdxs.push(key)
        return ObjectId(key)
      })

      questService.getAll({_id: {$in: qIds}}, (err, docs) => {
        if(err){
          callback(err)
        } else {
          docs.forEach(quest => {
            let index = qIdxs.indexOf(String(quest._id))
            if(index !== -1){
              let [,answer=''] = questions[index]
              let {question} = quest
              questions[index][1] = {question, answer}
            }
          })
          const quests = questions.map(([_id, value]) => {
            return Object.assign({_id}, value)
          })
          callback(null, quests)
        }
      })
    }
  })
}