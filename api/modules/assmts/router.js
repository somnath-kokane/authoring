
const Router = require('../../base/router')
const service = require('./service')
const router = Router({service})

router.get('/:id/questions', getQuestions)
router.put('/:id/questions', setQuestions)

module.exports = exports = router

function getQuestions(req, res, next) {
  let {id} = req.params
  service.getQuestions(id, (err, docs) => {
    if(err){
      next(err)
    } else {
      res.json(docs)
    }
  })
}

function setQuestions(req, res, next){
  let {id} = req.params
  let {questions} = req.body
  console.log('id', id, questions)
  service.setQuestions(id, questions, (err, doc) => {
    if(err){
      next(err)
    } else {
      res.json(doc)
    }
  })
}