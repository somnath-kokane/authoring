import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable()
export class RestService {
  state:any = []
  store = new BehaviorSubject<any>(this.state)
  userStore = new BehaviorSubject<any>({})
  userChanges = this.userStore.asObservable()
  stateChanges = this.store.asObservable()
  constructor(private http: Http) { }
  
  setHeaders(options:RequestOptions){
    let headers:Headers = options.headers || (options.headers = new Headers())
    headers.set('Content-Type', 'application/json')
    headers.set('charset', 'UTF-8')
    headers.set('X-Access-Token', localStorage.getItem('token'))
  }

  setState(state){
    this.store.next([...state])
  }

  request(method, uri, options?:RequestOptions){
    options = options || new RequestOptions()
    options.method = method
    this.setHeaders(options)

    return this.http.request(uri, options)
      .map((response: Response) => response.json());
  }

  get(uri, options?:RequestOptions, afterGet?){
    return this.request(RequestMethod.Get, uri, options)
      .do(res => {
        (afterGet ? afterGet(res) : this._afterGet(res))
      });
  }

  _afterGet(res){
    this.setState(res)
  }

  post(uri, body, options?:RequestOptions, afterPost?){
    options = options || new RequestOptions()
    options.body = JSON.stringify(body)
    return this.request(RequestMethod.Post, uri, options)
      .do(res => {
        (afterPost ? afterPost(res) : this._afterPost(res))
      })
  }

  protected _afterPost(res){
    if(res){
      const state = this.store.value
      const item = state.find(item => item._id === res._id)
      if(item){
        Object.assign(item, res)
      } else {
        state.push(res)
      }
      this.setState(state)
    }
    
  }

  put(uri, body, options?:RequestOptions, afterPut?){
    options = options || new RequestOptions()
    options.body = JSON.stringify(body)
    return this.request(RequestMethod.Put, uri, options)
      .do(res => {
        (afterPut ? afterPut(res) : this._afterPut(res))
      })
  }

  protected _afterPut(res){
    if(res){
      const state = this.store.value
      const item = state.find(item => item._id === res._id)
      if(item){
        Object.assign(item, res)
      }
      this.setState(state)
    }
  }

  remove(uri, options?:RequestOptions, afterDelete?){
    return this.request(RequestMethod.Delete, uri, options)
      .do(res => {
        (afterDelete ? afterDelete(res) : this._afterDelete(res))
      })
  }

  protected _afterDelete(res){
    const state:any[] = this.store.value
    let index = state.findIndex(item => item._id === res._id)
    if(index !== -1){
      state.splice(index, 1)
    }
    this.setState(state)
  }

}