import { Injectable } from '@angular/core';
import {RestService} from './rest.service'

@Injectable()
export class CandtService extends RestService{
  
  getAll(){
    return this.get('/api/candidates')
  }

  create(user){
    return this.post('/api/candidates', user)
  }

  update(id, user){
    return this.put(`/api/candidates/${id}`, user)
  }

  delete(id){
    return this.remove(`/api/candidates/${id}`)
  }

  getAssmts(id){
    return this.get(`/api/candidates/${id}/assessments`, null, () => {})
  }

  setAssmts(id, assessments){
    return this.put(`/api/candidates/${id}/assessments`, {assessments}, null, () => {
      let candt = (this.store.value || []).find(item => item._id === id)
      if(candt){
        candt.assessments = assessments
      }
    })
  }

  getQuests(id, assmtId){
    return this.get(`/api/candidates/${id}/assessments/${assmtId}/questions`, null, () => {})
  }

  getQuestions(id){
    return this.get(`/api/candidates/${id}/questions`, null, () => {})
  }

}