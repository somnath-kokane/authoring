
const jwt = require('jsonwebtoken')
const express = require('express')
const router = express.Router()
const userService = require('./modules/users/service')

process.on('unhandledRejection', (reason, p) => {
  console.log(`Possibly Unhandled Rejection at: Promise ${p} reason: ${reason}`);
});

process.on('uncaughtException', (err) => {
  console.log(`Caught exception: ${err.stack}\n`);
});

const privateKey = '37LvDSm4XvjYOh9Y';

router
  .use(verifyToken)
  .use('/users', require('./modules/users/router'))
  .use('/candidates', require('./modules/candts/router'))
  .use('/assessments', require('./modules/assmts/router'))
  .use('/questions', require('./modules/quests/router'))
  .use('/questions-categories', require('./modules/quests-catgry/router'))

exports.init = function (app) {
  app.use('/login', login)
  app.use('/api', router)
  app.use((err, req, res, next) => {
    console.log('erro.......\n', err.stack)
    res
      .status(500)
      .json(err)
  })
}

function login(req, res, next) {
  let { username, password } = req.body
  userService.getBy({ username }, (err, user) => {
    if (err) {
      next(err)
    } else if (user.password !== password) {
      next(err)
    } else {
      let userId = user._id
      let token = jwt.sign({ userId }, privateKey)
      res.json({ userId, token })
    }
  })
}

function verifyToken(req, res, next) {
  let token = req.body.token || req.query.token || req.headers['x-access-token'];
  console.log('token', token, req.headers)
  if (!token) {
    const err = new Error('invalid Token')
    next(err)
  } else {
    jwt.verify(token, privateKey, (err, decoded) => {
      if (err) {
        err.statusCode = 401
        next(err)
      } else {
        req.decoded = decoded
        next()
      }
    })
  }

}
