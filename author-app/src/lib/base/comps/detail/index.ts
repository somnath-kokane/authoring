import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'detail',
  templateUrl: 'detail.html'
})

export class DetailComponent implements OnInit {

  @Input() item: any
  @Input() can: any = {edit: false, delete: false}
  @Output() onEdited = new EventEmitter<any>()
  @Output() onDeleted = new EventEmitter<any>()

  fieldsets = []

  constructor() { }

  ngOnInit() { }

  ngOnChanges({can}){
    if(can && can.currentValue){
      let {edit=false} = can.currentValue
      Object.assign(can, {edit, delete: !!can.currentValue.delete})
    }
  }

  getClasses(count){
    let className = `col-md-${12/count}`
    return {[className]: true}
  }

  onEdit(item) {
    this.onEdited.emit(item)
  }

  onDelete(item) {
    this.onDeleted.emit(item)
  }
}