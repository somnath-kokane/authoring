import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import {combineLatest} from  'rxjs/observable/combineLatest';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs';

import {Ctrl} from '../../base/ctrl'

import {CandtService} from '../../services/candt.service'
import {AssmtService} from '../../services/assmt.service'

@Component({
  selector: 'candt-page',
  templateUrl: 'candt.html'
})

export class CandtPage extends Ctrl {
  assmts: Observable<any>
  quests: Observable<any>

  selectedAssmtSource = new BehaviorSubject<any>(null)
  selectedAssmt$ = this.selectedAssmtSource.asObservable()
  selectedAssmt:any
  selectedAssmts:any[] = []
  selectedCandt:any

  fieldsets = [
    [{label: 'Name', name: 'name'}],
    [{label: 'Username', name: 'username'}, {label: 'Password', value: '*****'}],
    [{label: 'Role', name: 'role'}, {label: 'Is Admin', name: 'isAdmin'}]
  ]

  constructor(
    protected service: CandtService,
    private assmtService: AssmtService
  ) { 

    super(service)

    this.items = this.items.map(items => {
      return items.map(item => Object.assign(item, item.user))
    })

    this.subscriptions.push(
      this.selectedItem$.subscribe(candt => {
        this.selectedAssmts = candt ? (candt.assessments || []) : []
      }),
      this.assmtService.getAll().subscribe()
    )

    this.assmts = this.assmtService.stateChanges
      
    this.quests = combineLatest(this.selectedItem$)
      .distinctUntilChanged()
      .switchMap(([candt]) => {
        return candt
          ? this.service.getQuestions(candt._id)
          : Observable.of([])
      })

  }

  onSelectAssmt({item, items}){
    console.log('items', items)
    if(item && item !== this.selectedAssmt){
      this.selectedAssmt = item
      this.selectedAssmtSource.next(item)
    }
    if(items){
      this.selectedAssmts = items
    }
  }

  setAssmts(){
    this.subscriptions.push(
      this.service.setAssmts(this.selectedCandt, this.selectedAssmts).subscribe()
    )
  }
  
}