const mongoose = require('mongoose');

const candtsSchema = new mongoose.Schema({
  assessments: Array,
  questions: Array,
  user:Object
});

const candts = mongoose.model('candidates', candtsSchema);

exports = module.exports = candts;
