import { Component, OnInit } from '@angular/core';

import {ListComponent} from '../base/comps/list'

@Component({
  selector: 'candt-list',
  templateUrl: '../base/comps/list/list.html'
})

export class CandtListComponent extends ListComponent {
  displayFields = [
    {label: 'Name', name: 'name', select: true}
  ]
}

@Component({
  selector: 'candt-quest-list',
  templateUrl: '../base/comps/list/list.html'
})
export class CandtQuestListComponent  extends ListComponent{
  
  displayFields = [
    {label: 'Question', name: 'question'},
    {label: 'Answer', name: 'answer'}
  ]

  can = {edit: false, delete: false}

}