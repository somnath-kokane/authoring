import { NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'
import {ReactiveFormsModule} from '@angular/forms'

import { UserListComponent } from './user-list';
import {UserFormComponent} from './user-form'
import {UserDetailComponent} from './user-detail'

@NgModule({
  imports: [BrowserModule, ReactiveFormsModule],
  exports: [UserListComponent, UserFormComponent, UserDetailComponent],
  declarations: [UserListComponent, UserFormComponent, UserDetailComponent]
})
export class QuestModule { }
