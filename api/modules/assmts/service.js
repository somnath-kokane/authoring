
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Types

const Service = require('../../base/service')
const Model = require('./model')
const questService = require('../quests/service')
const service = Service(Model, {getQuestions, setQuestions})

module.exports = exports = service

function getQuestions(id, callback) {
  service.get(id, (err, doc) => {
    if(err){
      callback(err)
    } else {
      const qIds = (doc.questions || []).map(quest => ObjectId(quest))
      questService.getAll({_id: {$in: qIds}}, callback)
    }
  })
}

function setQuestions(id, questions, callback){
  service.update(id, {questions}, callback)
}