import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import {AppService} from './app.service'

@Injectable()
export class AssmtService {
  store = new BehaviorSubject([])
  stateChanges:Observable<any>
  constructor(
    private http:Http,
    private appService: AppService
  ) {
    this.stateChanges = this.store.asObservable()
  }

  get state(): any{
    return this.store.value || []
  }

  getAssessments(){
    const token = this.appService.getToken()
    const headers = new Headers({
      'Content-Type': 'application/json', 'charset': 'UTF-8',
      'X-Access-Token': token
    });
    const options = new RequestOptions({ headers });
    this.http.get(
      `/api/assessments/`,
      options
    ).subscribe(res => {
      this.setState(res.json())
    })
  }

  addAssessment(assmt){
    const token = this.appService.getToken()
    const headers = new Headers({
      'Content-Type': 'application/json', 'charset': 'UTF-8',
      'X-Access-Token': token
    });
    const options = new RequestOptions({ headers });

    this.http.post(
      `/app/assessments/`,
      JSON.stringify(assmt),
      options
    ).subscribe(res => {
      const state = this.state
      state.push(res.json())
      this.setState(state)
    })
  }

  setState(state){
    this.store.next([...state])
  }

}