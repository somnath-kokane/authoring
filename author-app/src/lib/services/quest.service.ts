import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import {RestService} from './rest.service'

@Injectable()
export class QuestService extends RestService{
  
  constructor(
    http:Http
  ) {
    super(http)
  }

  getAll(){
    return this.get('/api/questions')
  }

  create(quest){
    return this.post('/api/questions', quest)
  }

  update(id, quest){
    return this.put(`/api/questions/${id}`, quest)
  }

  delete(id){
    return this.remove(`/api/questions/${id}`)
  }

}