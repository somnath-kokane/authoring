
const _ = require('lodash')
const mongoose = require('mongoose');
const {ObjectId} = mongoose.Types

const Router = require('../../base/router')
const service = require('./service')
const userService = require('../users/service')
const assmtService = require('../assmts/service')
const questService = require('../quests/service')

const router = Router({service})

router.get('/:id/assessments', getAssessments)
router.get('/:id/assessments/:assmtId/questions', getQuestions)
router.put('/:id/assessments/:assmtId/answers', setAnswers)
router.put('/:id/assessments', setAssmts)
router.get('/:id/questions', getQuests)

module.exports = exports = router

function setAnswers(req, res, next){
  let {id, assmtId} = req.params
  let questions = req.body
  service.setAnswers(id, assmtId, questions, (err, docs) => {
    if(err){
      next(err)
    } else {
      res.json(docs)
    }
  })
}

function getQuests(req, res, next){
  let {id} = req.params
  service.getQuests(id, (err, docs) => {
    if(err){
      next(err)
    } else {
      res.json(docs)
    }
  })
}

function setAssmts(req, res, next){
  let {id} = req.params
  let {assessments} = req.body
  service.setAssmts(id, assessments, (err, doc) => {
    if(err){
      next(err)
    } else {
      res.json(doc)
    }
  })
}

function getAssessments(req, res, next){
  let {id} = req.params
  service.getAssessments(id, (err, docs) => {
    if(err){
      next(err)
    } else {
      res.json(docs)
    }
  })
}

function getQuestions(req, res, next){
  let {id, assmtId} = req.params
  service.getQuestions(id, assmtId, (err, docs) => {
    if(err){
      next(err)
    } else {
      res.json(docs)
    }
  })
}

