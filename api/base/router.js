const express = require('express')
const Service = require('./service')

module.exports = exports = function ({service}) {
  
  const router = express.Router()
  
  router
    .route('/')
      .get(getAll)
      .post(create)
  router
    .route('/:id')
      .get(get)
      .put(update)
      .delete(remove)

  return router

  function getAll(req, res, next) {
    service.getAll({}, (err, docs) => {
      if(err){
        next(err)
      } else {
        res.json(docs)
      }
    })
  }

  function get(req, res, next){
    let {id} = req.params
    service.get(id, (err, doc) => {
      if(err){
        next(err)
      } else {
        res.json(doc)
      }
    })
  }

  function create(req, res, next){
    service.create(req.body, (err, doc) => {
      if(err){
        next(err)
      } else {
        res.json(doc)
      }
    })
  }

  function update(req, res, next){
    let {id} = req.params
    const data = req.body
    service.update(id, data, (err, doc) => {
      if(err){
        next(err)
      } else {
        res.json(Object.assign(doc, data))
      }
    })
  }

  function remove(req, res, next){
    let {id} = req.params
    service.remove(id, (err, doc) => {
      if(err){
        next(err)
      } else {
        res.json(doc)
      }
    })
  }
}