import { Component, OnInit } from '@angular/core';

import {DetailComponent} from '../base/comps/detail'

@Component({
  selector: 'candt-detail',
  templateUrl: '../base/comps/detail/detail.html'
})

export class CandtDetailComponent extends DetailComponent {
  fieldsets = [
    [{label: 'Name', name: 'name'}],
    [{label: 'Username', name: 'username'}, {label: 'Password', value: '*****'}],
    [{label: 'Role', name: 'role'}, {label: 'Is Admin', name: 'isAdmin'}]
  ]
}