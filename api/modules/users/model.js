const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  isAdmin: Boolean,
  role: String
});

const users = mongoose.model('users', usersSchema);

exports = module.exports = users;
