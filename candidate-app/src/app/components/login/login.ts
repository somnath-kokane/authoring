import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Observable } from 'rxjs/Observable';

import {UserService} from '../../user.service'
import {AppService} from '../../app.service'

@Component({
  selector: 'app-login',
  templateUrl: 'login.html',
  styles: []
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup
  user:Observable<any>
  app:Observable<any>

  constructor(
    public loginModalRef:BsModalRef,
    private userService: UserService,
    private appService: AppService,
    private fb:FormBuilder

  ) {
    this.buildLoginForm()
    this.user = this.userService.stateChanges
    this.app = this.appService.stateChanges
   }

  buildLoginForm(){
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  resetLoginForm(){
    this.loginForm.reset()
  }

  onSubmitLogin(){
    const user = this.loginForm.value
    this.loginForm.reset()
    this.loginModalRef.hide()
    this.userService.login(user)
  } 

  ngOnInit() {
  }

}
