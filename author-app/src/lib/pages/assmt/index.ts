import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import {combineLatest} from  'rxjs/observable/combineLatest';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TabsetComponent } from 'ngx-bootstrap';

import {Ctrl} from '../../base/ctrl'

import {AssmtService} from '../../services/assmt.service'
import {QuestService} from '../../services/quest.service'


@Component({
  selector: 'assmt-page',
  templateUrl: 'assmt.html'
})

export class Assmt extends Ctrl {

  questions:Observable<any>
  selectedQuestItems:any = []
  selectedQuestItem:any
  selectedAssmtId:any

  constructor(
    protected service: AssmtService,
    private questService: QuestService
  ) { 

    super(service)

    this.subscriptions.push(
      this.selectedItem$.subscribe(assmt => {
        if(assmt){
          this.selectedQuestItems = assmt.questions || []
        }
      }),
      this.questService.getAll().subscribe()
    )

    this.questions = this.questService.stateChanges
  }

  onSelectQuest({item, items}){
    if(item){
      this.selectedQuestItem = item
    }
    if(items){
      this.selectedQuestItems = items
    }
  }

  onEditQuest(item){}

  onDeleteQuest(item){}

  AddQuestsToAssmt(){
    console.log('assmtId', this.selectedAssmtId, this.selectedQuestItems)
    this.subscriptions.push(
      this.service.setQuestions(this.selectedAssmtId, this.selectedQuestItems).subscribe()
    )
  }

  setSelectedAssmt(ev){
    let assmtId = ev.target.value
    this.selectedAssmtId = assmtId
  }

  ngOnDestroy(){
    for(let subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }

  ngOnInit() { }
}