
module.exports = exports = function (Model, methods={}) {
  
  return Object.assign({getAll, get, getBy, create, update, remove}, methods)

  function getAll(conditions={}, callback) {
    Model.find(conditions, callback)
  }

  function get(id, callback){
    Model.findById(id, callback)
  }

  function getBy(conditions, callback){
    Model.findOne(conditions, callback)
  }

  function create(data, callback){
    Model.create(data, callback)
  }

  function update(id, data, callback){
    Model.findByIdAndUpdate(id, data, callback)
  }

  function remove(id, callback){
    Model.findByIdAndRemove(id, callback)
  }
}