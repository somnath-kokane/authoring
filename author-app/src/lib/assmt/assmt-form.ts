import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import { Observable } from 'rxjs/Observable';

import {FormComponent} from '../base/comps/form'

@Component({
  selector: 'assmt-form',
  templateUrl: '../base/comps/form/form.html'
})

export class AssmtFormComponent extends FormComponent {
  
  fields = {
    name: {value: '', required: true}
  }
  fieldsets = [
    ['name']
  ]
}