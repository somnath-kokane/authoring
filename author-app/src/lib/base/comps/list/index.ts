import { Component, OnInit, Input, Output, EventEmitter, Directive } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { Subscription } from 'rxjs/Subscription'

import {ConfirmComponent} from '../confirm-dialog'

@Component({
  selector: 'a-list',
  templateUrl: 'list.html'
})

export class ListComponent implements OnInit {
  subscriptions: Subscription[] = []
  
  selectAll = false
  selectedSource = new BehaviorSubject<any>({ selectedAll: false })
  selected$ = this.selectedSource.asObservable()
  itemsSource = new BehaviorSubject<any>([])
  items$ = this.itemsSource.asObservable()

  @Input() items: Observable<any>
  @Input() selectedItems = []
  @Input() can: any = {edit: false, delete: true, select: true, check: false}
  @Output() onSelected = new EventEmitter<any>();
  @Output() onEdited = new EventEmitter<any>()
  @Output() onDeleted = new EventEmitter<any>()

  private _items:any[] = []


  displayFields = []

  constructor(
  ) {

  }


  ngOnInit() {
    
  }

  ngOnChanges({ items, can, selectedItems }) {
    if(items && items.currentValue && items.currentValue !== items.previousValue){
      this.unsubscribeAll()
      this.subscriptions.push(
        items.currentValue.subscribe(items => {
          this.itemsSource.next(items)
        })
      )
    }
    if(can && can.currentValue){
      let _delete = true
      let {edit=true, select=true, check=true} = can.currentValue
      Object.assign(can, {edit, select, check, delete:_delete})
    }
    if(selectedItems && selectedItems.currentValue === undefined){
      selectedItems = []
    }
  }

  unsubscribeAll(){
    for (let subscription of this.subscriptions) {
      subscription.unsubscribe()
    }
  }

  ngOnDestroy() {
    this.unsubscribeAll()
  }

  onSelect(ev, item){
    ev.preventDefault();
    if(item){
      this.onSelected.emit({item: item})
    }
  }

  onEdit(item) {
    this.onEdited.emit(item)
  }

  onDelete(item) {
    this.onDeleted.emit(item)
  }

  onChangeCheckAll(ev) {
    let checked = ev.target.checked
    if(checked){
      this.selectedItems = this.itemsSource.value.map(item => item._id)
    } else {
      this.selectedItems = []
    }
    this.onSelected.emit({ items: this.selectedItems })
  }

  onChangeCheck(ev, item, count) {
    let checked = ev.target.checked
    let idx = this.selectedItems.indexOf(item._id)
    if (checked) {
      if (idx === -1) {
        this.selectedItems.push(item._id)
      }
    } else {
      if (idx !== -1) {
        this.selectedItems.splice(idx, 1)
      }
    }

    if (!this.selectedItems.length) {
      this.selectedSource.next({ selectedAll: false })
    } else if (this.selectedItems.length === count) {
      this.selectedSource.next({ selectedAll: true })
    }

    this.onSelected.emit({ items: this.selectedItems })
  }
}