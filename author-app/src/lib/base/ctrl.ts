import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import {combineLatest} from  'rxjs/observable/combineLatest';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

type IService = any

export class Ctrl {
  subscriptions:Subscription[] = []
  items:Observable<any>
  selectedItemSource = new BehaviorSubject<any>(null)
  selectedItem$:Observable<any> = this.selectedItemSource.asObservable()
  selectedItem:any
  deleteItem:any
  editItem:any
  showDeleteDialog:boolean = false
  showEditDialog:boolean = false

  constructor(
    protected service: IService
  ) { 
    this.items = this.service.stateChanges
    this.subscriptions.push(
      this.service.getAll().subscribe()
    )
  }

  onSubmit(item){
    let obs:Observable<any>
    if(item){
      if(item._id){
        obs = this.service.update(item._id, item)
      } else {
        obs = this.service.create(item)
      }
    }

    this.subscriptions.push(
      obs.subscribe()
    )
    
  }

  onSelect({item}){
    if(item){
      this.selectedItem  = item
      this.selectedItemSource.next(item)
    }
  }

  onEdit(item){
    this.editItem = item
    this.showEditDialog = true
  }

  onDelete(item){
    this.deleteItem = item
    this.showDeleteDialog = true
  }

  onDeleteConfirm(confirm){
    this.showDeleteDialog = false
    if(confirm){
      this.subscriptions.push(
        this.service.delete(this.deleteItem._id).subscribe(res => {
          this.deleteItem = null
        })
      )
    }
    
  }

  ngOnDestroy(){
    for(let subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }

  ngOnInit() { }
}