const mongoose = require('mongoose');

const assessmentsSchema = new mongoose.Schema({
  name: String,
});

const assessments= mongoose.model('assessments', assessmentsSchema);

exports = module.exports = assessments;
