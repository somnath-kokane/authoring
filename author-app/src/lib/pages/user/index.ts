import { Component, OnInit } from '@angular/core';

import {Ctrl} from '../../base/ctrl'

import {UserService} from '../../services/user.service'

@Component({
  selector: 'user-page',
  templateUrl: 'user.html'
})

export class UserPage extends Ctrl {
  constructor(
    protected service: UserService
  ) { 
    super(service)
  }

}