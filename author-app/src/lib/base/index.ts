
import {ConfirmComponent} from './comps/confirm-dialog'
import {
  PanelActions,
  PanelComponent,
  PanelTitle
} from './comps/panel'


import { NgModule } from '@angular/core';

const ALL_COMPS = [
  ConfirmComponent,
  PanelComponent,
  PanelTitle,
  PanelActions
]

@NgModule({
  imports: [],
  exports: [...ALL_COMPS],
  declarations: [...ALL_COMPS],
  providers: [],
  entryComponents: [ConfirmComponent]
})
export class BaseModule { }
