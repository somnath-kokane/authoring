import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {combineLatest} from 'rxjs/observable/combineLatest';
import {Subscription} from 'rxjs/Subscription'

import {ListComponent} from '../base/comps/list'

@Component({
  selector: 'assmt-quest-list',
  templateUrl: '../base/comps/list/list.html'
})

export class AssmtQuestListComponent extends ListComponent {
  
  displayFields = [
    {label: 'Question', name: 'question', select: true },
    {label: 'Category', name: 'categoryId'}
  ]
}