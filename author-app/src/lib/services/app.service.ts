import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

const defaultState = {
  user: {
    loggedIn: false,
  }
}

@Injectable()
export class AppService {
  store = new BehaviorSubject(defaultState)
  stateChanges:Observable<any>
  
  constructor(
    private http:Http
  ) { 
    this.stateChanges = this.store.asObservable()
  }

  login(user){
    const headers = new Headers({
      'Content-Type': 'application/json', 'charset': 'UTF-8'
    });
    const options = new RequestOptions({ headers });
    this.http.post(
      '/login/',
      JSON.stringify(user),
      options
    ).subscribe(res => {
      let {token, userId} = res.json()
      const user = {userId, token, loggedIn: true}
      localStorage.setItem('user', JSON.stringify(user))
      localStorage.setItem('token', token)
      this.setUser(user)
    })
  }

  logout(){
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    const user = {loggedIn: false}
    this.setUser(user)
  }

  get state(): any{
    return this.store.value || {}
  }

  getToken(){
    return this.state.user.token || ''
  }

  setUser(user){
    let state = this.state
    state.user = user
    this.setState(state)
  }

  setToken(token){
    const state = this.state
    state.user = state.user || {}
    state.user.token = token
  }

  setState(state){
    this.store.next({...state})
  }
}