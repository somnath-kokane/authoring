import { Component, OnInit } from '@angular/core';

import {FormComponent} from '../base/comps/form'

@Component({
  selector: 'candt-form',
  templateUrl: '../base/comps/form/form.html'
})

export class CandtFormComponent extends FormComponent {
  fields = {
    name: {required: true, label: 'Name'},
    username: {required: true, label: 'Username'},
    password: {type: 'password', required: true, label: 'Password'},
    isAdmin: {value: false, type: 'checkbox', disabled: true, required: true, label: 'Is Admin?'},
    role: {value: 'Candidate', type: 'select', disabled: true, required: true, options: ['Admin', 'Candidate'], label: 'Role'}
  }

  fieldsets = [
    ['name'],
    ['username', 'password'],
    ['isAdmin'],
    ['role']
  ]
}