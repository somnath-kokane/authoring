import { Component, OnInit, Output, Input, EventEmitter, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import {BsModalService} from 'ngx-bootstrap/modal'
import {Subscription} from 'rxjs/Subscription'

@Component({
  selector: 'confirm-dialog',
  templateUrl: 'confirm-dialog.html'
})

export class ConfirmComponent implements OnInit {

  subscriptions: Subscription[] = []

  modalRef: BsModalRef

  @Output() onConfirmed = new EventEmitter<any>()
  
  @ViewChild('modalTemplate') modalTemplate: TemplateRef<any>

  constructor(
    public modalService: BsModalService
  ) { 
    
  }

  ngOnInit() { 
    this.subscriptions.push(
      this.modalService.onHidden.subscribe(reason => {
        this.onConfirmed.emit(false)
      })
    )
    this.modalRef = this.modalService.show(this.modalTemplate, {class: 'modal-sm'})
  }

  ngOnChanges(){
    
  }

  ngOnDestroy(){
    for(let subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }

  onNo(){
    this.onConfirmed.emit(false)
    this.modalRef.hide()
  }

  onYes(){
    this.onConfirmed.emit(true)
    this.modalRef.hide()
  }

}