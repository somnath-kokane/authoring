import { Component, OnInit } from '@angular/core';

import {FormComponent} from '../base/comps/form'

@Component({
  selector: 'quest-form',
  templateUrl: '../base/comps/form/form.html'
})

export class QuestFormComponent extends FormComponent {
  
  fields = {
    question: {type:'textarea', required: true, label:'Question'},
    category: {type: 'select', required: true, label: 'Category', options: ['UI/UX', 'JAVA', '.NET', 'JavaScript']}
  }

  fieldsets = [
    ['question'],
    ['category']
  ]

}