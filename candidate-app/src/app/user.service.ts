import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http'
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import {AppService} from './app.service'

@Injectable()
export class UserService {
  store:BehaviorSubject<any>
  stateChanges:Observable<any>
  state = {
    loggedIn: false,
    user: undefined,
    loading: false
  }
  
  constructor(
    private appService: AppService,
    private http:Http
  ) { 
    this.store = new BehaviorSubject(this.state)
    this.stateChanges = this.store.asObservable()
  }

  logout(){
    localStorage.removeItem('userId')
    localStorage.removeItem('token')
    this.appService.setUser({user: null, loggedIn: false})
  }

  login(user){
    
    const headers = new Headers({ 'Content-Type': 'application/json', 'charset': 'UTF-8' });
    const options = new RequestOptions({ headers });
    this.http.post(
      '/login/',
      JSON.stringify(user),
      options
    ).subscribe(res => {
      let {token, userId} = res.json()
      user = {token, userId}
      localStorage.setItem('userId', userId)
      localStorage.setItem('token', token)
      this.appService.setUser({user, loggedIn: true})
    })
    
  }

}
