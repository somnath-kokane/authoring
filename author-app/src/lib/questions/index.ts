import { NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser'
import {ReactiveFormsModule} from '@angular/forms'

import { QuestListComponent } from './quest-list';
import {QuestFormComponent} from './quest-form'
import {QuestDetailComponent} from './quest-detail'

@NgModule({
  imports: [BrowserModule, ReactiveFormsModule],
  exports: [QuestListComponent, QuestFormComponent, QuestDetailComponent],
  declarations: [QuestListComponent, QuestFormComponent, QuestDetailComponent]
})
export class QuestModule { }
