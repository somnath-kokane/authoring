
import { Component, OnInit } from '@angular/core';

import {DetailComponent} from '../base/comps/detail'

@Component({
  selector: 'quest-detail',
  templateUrl: '../base/comps/detail/detail.html'
})

export class UserDetailComponent extends DetailComponent {
  
  fieldsets  = [
    [{label: 'Name', name: 'name'}],
    [{label: 'Username', name: 'usename'}, {label: "Password", name: 'password'}],
    [{label: 'isAdmin', name: 'isAdmin'}],
    [{label: 'Role', name: 'role'}]
  ]

}