import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import { TabsModule } from 'ngx-bootstrap/tabs';

import {LoginComponent} from './login/login'

import {
  AppService,
  AssmtService,
  QuestService,
  UserService,
  CandtService
} from './services'

import {APP_ROUTES} from './routes'

import {AssmtModule} from './assmt'
import {QuestModule} from './questions'
import {CandtModule} from './candts'
import {BaseModule} from './base'

import {AuthorApp} from './author'
import {Home} from './pages/home'
import {Assmt} from './pages/assmt'
import {CandtPage} from './pages/candt'
import {QuestPage} from './pages/quest'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(APP_ROUTES),
    AssmtModule,
    QuestModule,
    CandtModule,
    BaseModule,
    TabsModule
    
  ],
  exports: [LoginComponent],
  declarations: [
    LoginComponent,
    AuthorApp,
    Home,
    Assmt,
    CandtPage,
    QuestPage
  ],
  entryComponents: [LoginComponent],
  providers: [AppService, AssmtService, QuestService, UserService, CandtService],
  // schemas: [NO_ERRORS_SCHEMA]
})
export class AuthorModule { }
