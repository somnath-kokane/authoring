
import { Component, OnInit } from '@angular/core';

import {Ctrl} from '../../base/ctrl'

import {QuestService} from '../../services/quest.service'

@Component({
  selector: 'quest-page',
  templateUrl: 'quest.html'
})

export class QuestPage extends Ctrl {
  
  constructor(
    protected service: QuestService
  ) {
    super(service)
  }

}