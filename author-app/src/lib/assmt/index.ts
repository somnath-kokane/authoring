import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms'
import { AssmtComponent } from './assmt';
import {AssmtFormComponent} from './assmt-form'
import {AssmtListComponent} from './assmt-list'
import {AssmtQuestListComponent} from './quest-list'
import {AssmtDetailComponent} from './assmt-detail'

@NgModule({
  imports: [ReactiveFormsModule, BrowserModule],
  exports: [
    AssmtComponent, AssmtListComponent, 
    AssmtQuestListComponent, AssmtDetailComponent, AssmtFormComponent],
  declarations: [
    AssmtFormComponent, AssmtListComponent,
    AssmtQuestListComponent, AssmtDetailComponent, AssmtComponent],
  providers: [],
})
export class AssmtModule { }
