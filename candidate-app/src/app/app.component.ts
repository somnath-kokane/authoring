import { Component, OnInit, ViewChild} from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import {combineLatest} from 'rxjs/observable/combineLatest';

import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/distinctUntilChanged';

import {AppService} from './app.service'
import {AssmtsService} from './assmts.service'
import {UserService} from './user.service'

import {LoginComponent} from './components/login/login'
import {LoadingComponent} from './components/loading/loading'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html' ,
  styles: []
})
export class AppComponent implements OnInit {
  title = 'app';
  selectedAssmt: Observable<any>
  selectedAssmtSource = new BehaviorSubject({})
  loginModalRef:BsModalRef
  loadingModalRef:BsModalRef
  app:Observable<any>
  assmts:Observable<any>
  questions:Observable<any>
  alertMessages:Observable<any>
  questionsSource: BehaviorSubject<any>

  constructor(
    private bsModalService: BsModalService,
    private appService: AppService,
    private assmtsService: AssmtsService,
    private userService: UserService
  ){
    this.app = this.appService.stateChanges
    this.assmts = this.assmtsService.stateChanges
    this.selectedAssmt = this.selectedAssmtSource.asObservable()

    this.alertMessages = this.appService.stateChanges.pluck('alertMessages').distinctUntilChanged()

    this.questions = combineLatest(
      this.selectedAssmt,
      this.assmts,
      (assmt) => {
        return assmt.questions || []
      })

    let userId = localStorage.getItem('userId')
    let token = localStorage.getItem('token')
    if(userId){
      const user = {userId, token}
      this.appService.setUser({user, loggedIn:true})
    }
      
  }

  removeAlertMessage(msgObj){
    this.appService.removeAlertMessages(msgObj)
  }

  ngOnInit() {
    this.appService.stateChanges
      .pluck('loading')
      .subscribe(loading => {
        if(loading){
          console.log('loading....', loading)
          this.loadingModalRef = this.bsModalService.show(LoadingComponent, {class: 'modal-sm'})
        } else if(this.loadingModalRef){
          this.loadingModalRef.hide()
        }
      })  
  }

  onAssessments(ev){
    ev.preventDefault()
    const user = this.appService.getUser()
    this.assmtsService.getAssmts(user.userId)
  }

  getQuestions(ev, assmt){
    ev.preventDefault()
    this.assmtsService.getQuestions(assmt._id)
    this.selectedAssmtSource.next(assmt)
  }

  submitAnswers(){
    let selectedAssmt:any = this.selectedAssmtSource.value
    let assessmentId = selectedAssmt._id
    let questions = selectedAssmt.questions
    this.assmtsService.answerQuestions(assessmentId, questions)
  }

  onLogout(ev){
    ev.preventDefault()
    // this.appService.setUser({loggedIn: false})
    this.userService.logout()
  }

  onLogin(ev){
    ev.preventDefault();
    this.loginModalRef = this.bsModalService.show(LoginComponent, {class: 'modal-sm'})
  }
}
