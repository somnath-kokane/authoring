import { Component, OnInit, Directive } from '@angular/core';

@Directive({
  selector: 'panel-title'
})
export class PanelTitle {}

@Directive({
  selector: 'panel-actions'
})
export class PanelActions {}

@Component({
  selector: 'panel',
  templateUrl: 'panel.html'
})

export class PanelComponent implements OnInit {
  constructor() { }

  ngOnInit() { }
}