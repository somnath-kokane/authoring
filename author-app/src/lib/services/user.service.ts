import { Injectable } from '@angular/core';
import {RestService} from './rest.service'

@Injectable()
export class UserService extends RestService{
  
  getAll(){
    return this.get('/api/users')
  }

  create(user){
    return this.post('/api/users', user)
  }

  update(id, user){
    return this.put(`/api/users/${id}`, user)
  }

  remove(id){
    return this.remove(`/api/users/${id}`)
  }

}