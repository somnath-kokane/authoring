import { Component, OnInit } from '@angular/core';

import {DetailComponent} from '../base/comps/detail'

@Component({
  selector: 'assmt-detail',
  templateUrl: '../base/comps/detail/detail.html'
})

export class AssmtDetailComponent extends DetailComponent {
  
  fieldsets  = [
    [{label: 'Name', name: 'name'}]
  ]

}