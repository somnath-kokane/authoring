import { Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import {combineLatest} from 'rxjs/observable/combineLatest'
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';


import {AssmtService} from '../services/assmt.service'

@Component({
  selector: 'assmt',
  templateUrl: 'assmt.html'
})

export class AssmtComponent implements OnInit {
  subscriptions = []
  items:Observable<any>
  questions:Observable<any>
  selectedSource = new Subject<any>()
  selected$ = this.selectedSource.asObservable()
  selected:any
  selectedQuests:any[] = []

  constructor(
    private service: AssmtService,
  ) { 

    this.items = this.service.stateChanges
    this.subscriptions.push(
      this.service.getAll().subscribe()
    )

    this.questions = combineLatest(this.selected$)
    .do(([selected]) => {
        this.selected = selected
    })
    .switchMap(res => {
      return this.selected 
        ? this.service.getAllQuestions(this.selected) 
        : Observable.of([])
    })
  }

  onCancel(){
    this.selectedSource.next(null)
  }

  onSelect(item){
    this.selectedSource.next(item)
  }

  onSelectQuestions({items}){
    console.log('items', items)
    this.selectedQuests = items
  }

  onAdd(){
    this.selected = null
  }

  onEdit(item){
    this.selectedSource.next(item)
  }

  onDelete(item){
    this.subscriptions.push(
      this.service.delete(item._id).subscribe(res => {
        if(this.selected && this.selected._id === item._id){
          this.selected = null
        }
      })
    )
  }

  onSubmit(item){
    let createOrUpdate
    if(this.selected){
      createOrUpdate = this.service.update(this.selected._id, item)
      this.selected = null
    } else {
      createOrUpdate = this.service.create(item)
    }
    this.subscriptions.push(
      createOrUpdate.subscribe()
    )
  }

  ngOnDestroy(){
    for(let subscription of this.subscriptions){
      subscription.unsubscribe()
    }
  }

  ngOnInit() { }
}