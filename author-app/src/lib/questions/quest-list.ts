import { Component, OnInit } from '@angular/core';

import {ListComponent} from '../base/comps/list'

@Component({
  selector: 'quest-list',
  templateUrl: '../base/comps/list/list.html'
})

export class QuestListComponent extends ListComponent {

  displayFields = [
    {label: 'Question', name: 'question', select: true },
    {label: 'Category', name: 'categoryId'}
  ]
}